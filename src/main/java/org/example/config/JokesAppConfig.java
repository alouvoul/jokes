package org.example.config;


public abstract class JokesAppConfig {

    public static final String JOKESFILEPATH = "/src/main/resources/jokes";

    public static final String ABSOLUTEPATH = System.getProperty("user.dir");

}
