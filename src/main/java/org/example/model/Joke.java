package org.example.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Data
public class Joke {

    private Category jokeCategory;

}
