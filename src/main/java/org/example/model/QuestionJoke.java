package org.example.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class QuestionJoke extends Joke {

    private String question;

    private String answer;

    public QuestionJoke(String question, String answer, Category jokeCategory) {
        super(jokeCategory);
        this.question = question;
        this.answer = answer;
    }

}
