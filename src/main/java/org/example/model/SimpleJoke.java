package org.example.model;

import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = true)
public class SimpleJoke extends Joke{

    private String description;

    public SimpleJoke(String description, Category category) {
        super(category);
        this.description = description;
    }
}
