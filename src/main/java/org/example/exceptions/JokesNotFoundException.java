package org.example.exceptions;

public class JokesNotFoundException extends Exception{
    public JokesNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
