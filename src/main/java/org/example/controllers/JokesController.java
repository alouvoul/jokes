package org.example.controllers;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.example.model.Category;
import org.example.model.Joke;
import org.example.service.ReaderService;
import org.example.service.file.FileReaderServiceImpl;

import java.util.*;

@EqualsAndHashCode
@Data
public class JokesController<K extends Joke> implements Jokes {


    private List<K> jokes;

    private final Random random = new Random();

    private final ReaderService readerService = new FileReaderServiceImpl();

    public JokesController() {

        try {
            jokes = readerService.getJokes();
        } catch (Exception e){
            System.out.println("Couldn't find the file");
        }

    }

    @Override
    public K getRandomJoke() {
        return this.jokes.get(random.nextInt(this.jokes.size()));
    }

    @Override
    public HashMap<String, String> getJokeWithSpecificText(String text) {

        //TODO Implement this functionality
        return null;
    }

    @Override
    public HashSet<Joke> getJokesFromCategory(Category category) {
        return null;
    }

}
