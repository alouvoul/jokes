package org.example.controllers;

import org.example.model.Category;
import org.example.model.Joke;

import java.util.HashMap;
import java.util.HashSet;

public interface Jokes {

    Joke getRandomJoke();

    HashMap<String, String> getJokeWithSpecificText(String text);

    HashSet<Joke> getJokesFromCategory(Category category);

}
