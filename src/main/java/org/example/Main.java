package org.example;

import java.util.logging.Logger;
import java.util.logging.Level;
import org.example.model.Category;
import org.example.model.Joke;
import org.example.model.QuestionJoke;
import org.example.model.SimpleJoke;
import org.example.service.file.FileReaderServiceImpl;

import java.io.IOException;
import java.util.ArrayList;

public class Main {

    static Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) {

        ArrayList<Joke> computerJokes = new ArrayList<>();
        computerJokes.add(new SimpleJoke("Chuck norris errors are only FATAL!", Category.COMPUTER));
        computerJokes.add(new QuestionJoke("Ti einai koino se ena krevati kai ena moluvi",
                "Kai ta duo den kanoun podilato", Category.CAT));


        for (Joke joke : computerJokes) {
            logger.log(Level.INFO, joke.toString());
        }

        try{
            FileReaderServiceImpl readerService = new FileReaderServiceImpl();
            readerService.getJokes();
        } catch (IOException ioException){
            logger.log(Level.INFO, "Couldn't load data");
        }

    }
}