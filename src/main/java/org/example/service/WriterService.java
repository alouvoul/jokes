package org.example.service;

import java.io.IOException;
import java.util.List;

public interface WriterService<T> extends BaseService {

    T save(T object);

}
