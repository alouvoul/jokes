package org.example.service.map;

import org.example.model.Joke;
import org.example.service.ReaderService;
import org.example.service.WriterService;

import java.util.List;

public class MapReaderService implements ReaderService<Joke, String>, WriterService<Joke> {

    private final List<Joke> jokes;

    public MapReaderService(List<Joke> jokes) {
        this.jokes = jokes;
    }

    @Override
    public List<Joke> getJokes() {
        return jokes;
    }

    @Override
    public List<Joke> getJokesFromCategory(String value) throws Exception {
        return null;
    }


    @Override
    public Joke save(Joke object) {
        jokes.add(object);
        return object;
    }
}
