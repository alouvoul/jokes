package org.example.service.file;

import org.example.config.JokesAppConfig;
import org.example.exceptions.JokesNotFoundException;
import org.example.model.Category;
import org.example.model.Joke;
import org.example.model.QuestionJoke;
import org.example.model.SimpleJoke;
import org.example.service.ReaderService;
import org.example.service.Utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileReaderServiceImpl implements ReaderService<Joke, String> {

    private final Utils utils = new Utils();

    @Override
    public List<Joke> getJokes() throws IOException{
        JokesAppConfig jokesAppConfig = new JokesAppConfig();

        List<String> result = new FileReaderUtil()
                .readFromInputStream(
                        new FileInputStream(jokesAppConfig.getAbsolutePath() + jokesAppConfig.getPath()));
        return utils.getJokeObjectsFromList(result);
    }

    @Override
    public List<Joke> getJokesFromCategory(String value) throws Exception {
        return null;
    }


}
