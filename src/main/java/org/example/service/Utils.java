package org.example.service;


import org.example.model.Category;
import org.example.model.Joke;
import org.example.model.QuestionJoke;
import org.example.model.SimpleJoke;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

public class Utils {

    public List<Joke> getJokeObjectsFromList(List<String> jokes) {

        List<Joke> objects = new ArrayList<>();

        jokes.stream().forEach(joke -> {
            if (joke.split(";;")[1].equals("")) {
                objects.add(new SimpleJoke(joke.split(";;")[0], Category.valueOf(joke.split(";;")[2])));
            } else {
                System.out.println(joke);
                objects.add(new QuestionJoke(joke.split(";;")[0], joke.split(";;")[1], Category.valueOf(joke.split(";;")[2])));
            }
        });

        return objects;
    }

}
