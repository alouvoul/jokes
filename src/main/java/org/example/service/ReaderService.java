package org.example.service;


import org.example.exceptions.JokesNotFoundException;

import java.io.IOException;
import java.util.List;

public interface ReaderService<K, E> extends BaseService {

    List<K> getJokes() throws Exception;

    List<K> getJokesFromCategory(E value) throws Exception;

}
